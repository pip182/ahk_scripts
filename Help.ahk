
;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                         Help Menu                                                        >
;<-------------------------------------------------------------------------------------------------------------------------->

^#F1::
{
  Gui, 99:Font, s11 Bold Underline
  Gui, 99:Add, Text, % "x10", Quick decimals on numberpad:
  Gui, 99:Font, s8 Normal

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2",  Win + 1 : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Sends 0.0625

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + 2 : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Sends 0.125

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + 3 : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Sends 0.25

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + 4 : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Sends 0.375

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + 5 : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Sends 0.5

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + 6 : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Sends 0.625

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + 7 : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Sends 0.75

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + 8 : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Sends 0.825

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + 9 : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Sends 0.9375

  Gui, 99:Font, s11 Bold Underline
  Gui, 99:Add, Text, % "x10", Autocad Shortcuts:
  Gui, 99:Font, s8 Normal

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + ( NumPad + ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Turn all ( Autocad ) layers on

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + ( NumPad - ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Turn off layer by selecting object.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( L ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Draw a line.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( O ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Offset command.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( P ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Polyline edit command.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( M ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Move object.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( C ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Copy object.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( D ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Quick linear dimension.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( E ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Explode object.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( T ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Trim command.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( `; ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Do a 3D Polyline.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( Z ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Reset AutoCAD coordinates.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( NumPad - ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Hide Objects.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( NumPad * ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Isolate Object ( Hide everything but selected object ).

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( NumPad + ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Unhide/Unisolate objects.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + [ : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Slow Autocad Zoom.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ] : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Normal Autocad Zoom.

	Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + Z : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Reset World Coordinates.

	Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Win + Z : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Reset View Coordinates.

	Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Alt + Z : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, South West View.

	Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ] : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Normal Autocad Zoom.


;<----------------------------------------------------------------------------->
;<                                 Other Stuff                                 >
;<----------------------------------------------------------------------------->


  Gui, 99:Font, s11 Bold Underline
  Gui, 99:Add, Text, % "x10", Other Commands:
  Gui, 99:Font, s8 Normal

  ;Gui, 99:Font, Bold
  ;Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + F5 : %A_Space%
  ;Gui, 99:Font, Normal
  ;Gui, 99:Add, Text, X+0, Open Virtual Nerd.

	Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Alt + Left Mouse Button : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Open note editor.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + ( - ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Create a banner with custom text ( Only useful in text editors ).

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + ( A ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Go to beggining of line.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + ( E ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Go to end of line.

  Gui, 99:Font, Bold
  Gui, 99:Add, Text, % "x30 y+2 ",  Ctrl + Win + ( Enter ) : %A_Space%
  Gui, 99:Font, Normal
  Gui, 99:Add, Text, X+0, Press 'Enter' x amount of times.

  Gui, 99:+AlwaysOnTop
  Gui, 99:Show,, Hot Key Reference
  return


  99GuiClose:
  99GuiEscape:
  {
    Gui, 99:Destroy
    return
  }
}
