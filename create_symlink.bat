@echo off

REM Create some variables
SET source=O:\Departments\IT\HotKeys\AutoHotkey.ahk
SET destination=D:\My Documents\AutoHotkey.ahk
SET errorText=Check that source and destination exist and that you have ran this script as Administrator.  Also make sure that there is not already an AutoHotKey.ahk file in My Documents.
SET successText=It seems like the link was created! You will need to reload AutoHotKey for it to take effect.

DEL "%destination%"

MKLINK "%destination%" "%source%"

if errorlevel 1 (
	echo.
	echo Could not create link.
	echo Source: %source%
	echo Destination: %destination%
	echo.
	echo %errorText%
	msg * %errorText%
) else (
	echo.
	echo %successText% 
	msg * %successText%
)
echo.
pause