

; My Microvellum 7 helper script. Provides easy access to common forumlas and commands.

; Reference:
; Pop up message on top of all toher windows.
;  MsgBox, 4096, Test

;Gui, Destroy

;#Include O:\Departments\IT\HotKeys\FindClick.ahk

Width := 375
Guixpos := A_ScreenWidth - Width - 20
Gui, +LastFound
WinSet, Transparent, 220
Gui, Color, 808080
Gui, Margin, 0, 0
Gui, Font, s12 cFEFEFE Bold
Gui, Add, Progress, % "x-1 y-1 w" (Width+2) " h31 Background404040 Disabled hwndHPROG"
Gui, Add, Text, % "x0 y0 w" Width " h30 BackgroundTrans Center 0x200 gGuiMove vCaption", Microvellum Monkey
Gui, Font, s10
Gui, Add, Text, % "x26 y+10 w" (Width-14) "r1 cWhite vTX1 gRefresh", Refresh Specs
Gui, Add, Text, % "x26 y+10 w" (Width-14) "r1 cWhite vTX2 gPick", Pick Image
;Gui, Add, Text, % "x26 y+10 w" (Width-14) "r1 cWhite vTX5 gShipping", Print Shipping Labels (Ctrl+Win+1)
;Gui, Add, Text, % "x26 y+10 w" (Width-14) "r1 cWhite vTX10 gDrawers", Create buyout drawer report (Ctrl+Win+2)
;Gui, Add, Text, % "x26 y+10 w" (Width-14) "r1 cWhite vTX1 gClear", Clear part selection (Ctrl+Win+3)
;Gui, Add, Text, % "x26 y+10 w" (Width-14) "r1 cWhite vTX2 gPartLabels", Make Nested Part Labels (Ctrl+Win+4)

;Gui, Add, Text, % "x26 y+10 w" (Width-14) "r1 cWhite vTX2 gPartLabels", Make Nested Part Labels (Ctrl+Win+4)
;Gui, Add, Text, % "x26 y+10 w" (Width-14) "r1 cWhite vTX2 gPartLabels", Make Nested Part Labels (Ctrl+Win+4)

Gui, Add, Text, % "x12 y+16 w" (Width-14) "r1 Center vTX4 cEF7800 gClose", Close
Gui, Add, Text, % "x12 y+10 w" (Width-14) "h5 vP"
GuiControlGet, P, Pos
H := PY + PH
Gui, -Caption +AlwaysOnTop
WinSet, Region, 0-0 w%Width% h%H% r6-6
Gui, Show, % "y20 x" Guixpos "w" Width
return

GuiMove:
  PostMessage, 0xA1, 2
return

Pick:
{
  ;FindClick()
  Return
}


Refresh:
{
  WinWait, Toolbox
  WinActivate, Toolbox
  Click, 211, 257 Left, Down
  Click, 211, 257 Left, Up
  Send, {Down}{Down}{Down}{Down}{Down}{Enter}
  WinWait, User Access Denied. Enter Administrator Credentials...
  WinActivate, User Access Denied. Enter Administrator Credentials...
  Send, {Tab}{LShift Down}{a}{LShift Up}{d}{m}{i}{n}{Down}{Tab}{1}{a}{d}{m}{i}{n}{2}
  Click, 39, 116 Left, Down
  Click, 39, 116 Left, Up
  WinWait, Advanced Library Setup
  WinActivate, Advanced Library Setup
  Click, 83, 42 Left, Down
  Click, 83, 42 Left, Up
  Click, 83, 42 Right, Down
  Click, 83, 42 Right, Up
  Click, 177, 117 Left, Down
  Click, 177, 117 Left, Up
  WinWait, Project Wizard
  WinActivate, Project Wizard
  Click, 50, 632 Left, Down
  Click, 50, 632 Left, Up
  WinWait, Advanced Library Setup
  WinActivate, Advanced Library Setup
  Click, 104, 35 Right, Down
  Click, 104, 35 Right, Up
  Click, 161, 137 Left, Down
  Click, 161, 137 Left, Up
  WinWait, Specification Group Name: Default Spec Group - Template
  WinActivate, Specification Group Name: Default Spec Group - Template
  Click, 875, 555 Left, Down
  Click, 875, 555 Left, Up
  WinWait, Advanced Library Setup
  WinActivate, Advanced Library Setup
  Click, 198, 422 Left, Down
  Click, 198, 422 Left, Up
  WinWait, Toolbox
  WinActivate, Toolbox
  Click, 225, 258 Left, Down
  Click, 225, 258 Left, Up
  Send, {Down}{Down}{Down}{Down}{Down}{Down}{Enter}
  WinWait, Advanced Project Setup
  WinActivate, Advanced Project Setup
  Click, 135, 40 Left, Down
  Click, 135, 40 Left, Up
  Click, 135, 40 Right, Down
  Click, 135, 40 Right, Up
  Click, 187, 292 Left, Down
  Click, 187, 292 Left, Up
  Click, 527, 296 Left, Down
  Click, 527, 296 Left, Up
  ; Wizard
  WinWait, Specification Group Components
  WinActivate, Specification Group Components
  Click, 27, 56 Left, Down
  Click, 27, 56 Left, Up
  Click, 37, 77 Left, Down
  Click, 38, 77 Left, Up
  Click, 102, 90 Left, Down
  Click, 102, 90 Left, Up
  Click, 42, 409 Left, Down
  Click, 42, 407 Left, Up
  WinWait, Microvellum
  WinActivate, Microvellum
  Click, 174, 150 Left, Down
  Click, 174, 150 Left, Up
  WinWait, Advanced Project Setup
  WinActivate, Advanced Project Setup
  Click, 249, 36 Right, Down
  Click, 249, 36 Right, Up
  Click, 335, 279 Left, Down
  Click, 336, 279 Left, Up
  Click, 659, 316 Left, Down
  Click, 659, 316 Left, Up
  ; Globals
  WinWait, Specification Group Components
  WinActivate, Specification Group Components
  Click, 23, 56 Left, Down
  Click, 23, 56 Left, Up
  Click, 42, 72 Left, Down
  Click, 41, 72 Left, Up
  Click, 83, 104 Left, Down
  Click, 84, 104 Left, Up
  Click, 57, 413 Left, Down
  Click, 57, 413 Left, Up
  WinWait, Microvellum
  WinActivate, Microvellum
  Click, 290, 131 Left, Down
  Click, 290, 131 Left, Up
  WinWait, Advanced Project Setup
  WinActivate, Advanced Project Setup
  Click, 164, 40 Right, Down
  Click, 164, 40 Right, Up
  Click, 308, 290 Left, Down
  Click, 308, 290 Left, Up
  Click, 528, 339 Left, Down
  Click, 528, 339 Left, Up
  ;Cut Parts
  WinWait, Specification Group Components
  WinActivate, Specification Group Components
  Click, 22, 63 Left, Down
  Click, 22, 63 Left, Up
  Click, 39, 70 Left, Down
  Click, 39, 70 Left, Up
  Click, 83, 88 Left, Down
  Click, 83, 88 Left, Up
  Click, 47, 409 Left, Down
  Click, 47, 409 Left, Up
  WinWait, Microvellum
  WinActivate, Microvellum
  Click, 301, 137 Left, Down
  Click, 301, 137 Left, Up
  WinWait, Advanced Project Setup
  WinActivate, Advanced Project Setup
  Click, 164, 40 Right, Down
  Click, 164, 40 Right, Up
  Click, 351, 298 Left, Down
  Click, 351, 297 Left, Up
  Click, 591, 354 Left, Down
  Click, 591, 354 Left, Up
  ; Edgebanding
  WinWait, Specification Group Components
  WinActivate, Specification Group Components
  Click, 24, 56 Left, Down
  Click, 24, 56 Left, Up
  Click, 31, 76 Left, Down
  Click, 31, 76 Left, Up
  Click, 43, 75 Left, Down
  Click, 43, 75 Left, Up
  Click, 82, 94 Left, Down
  Click, 80, 96 Left, Up
  Click, 29, 409 Left, Down
  Click, 29, 409 Left, Up
  WinWait, Microvellum
  WinActivate, Microvellum
  Click, 284, 128 Left, Down
  Click, 284, 128 Left, Up
  WinWait, Advanced Project Setup
  WinActivate, Advanced Project Setup
  Click, 64, 412 Left, Down
  Click, 64, 412 Left, Up
  Return
}



;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                      Close The App                                                       >
;<-------------------------------------------------------------------------------------------------------------------------->

Close:
{
  Gui, Destroy
  return
}
