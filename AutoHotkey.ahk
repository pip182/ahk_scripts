#SingleInstance Force
#InstallKeybdHook
#Persistent

#Hotstring ? *

#Include O:\Departments\IT\HotKeys\HotKeys.ahk
#Include O:\Departments\IT\HotKeys\Help.ahk
#Include O:\Departments\IT\HotKeys\LazyCalc.ahk
#include O:\Departments\IT\HotKeys\Note_Ed.ahk
;#Include HotKeys.ahk
;#Include Help.ahk


;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                     Open Virtual Nerd                                                    >
;<-------------------------------------------------------------------------------------------------------------------------->


;^#F5::
;	#Include O:\Departments\IT\HotKeys\MV_Helper.ahk
;	#Include MV_Helper.ahk
;return


;<----------------------------------------------------------------------------->
;<                                 Date Picker                                 >
;<----------------------------------------------------------------------------->
; Pops up a date picker dialog with todays date pre-selected.

SetTitleMatchMode RegEx

IfWinNotActive ahk_exe PuTTYNG
{
	^;::
	::ddd::
	  Gui, Add, MonthCal, vDate
	  Gui, Add, Button, Default, OK
	  Gui, Show, x800 y200
	return

	ButtonOK:
	  Gui, Submit
	  Gui, Destroy
	  FormatTime, date, %date%, dddd, MMM dd
	  SendInput, %date%
	return

	GuiClose:
		Gui, Destroy
	return
}

#IfWinNotActive



;<----------------------------------------------------------------------------->
;<                             Automatic Typo Fixer                            >
;<----------------------------------------------------------------------------->


::chnage::change


;<----------------------------------------------------------------------------->
;<                           Random phrase generator                           >
;<----------------------------------------------------------------------------->


::rrrr::

; Just a whole bunch of random words to pick from
words := ["monkey","count","a","able","about","across","after","well","all","almost","also","am","among","be","car"
	   ,"an","and","any","are","aren't","as","at","be","because","been","but","by","can","can't","cannot","hand"
       ,"could","could've","couldn't","dear","did","didn't","do","does","doesn't","don't","either","else","foot"
	   ,"ever","every","for","from","get","got","had","has","hasn't","have","he","he'd","he'll","he's","crunchy"
	   ,"her","hers","him","his","how","how'd","how'll","how's","however","i","i'd","i'll","i'm","i've","slam"
	   ,"if","in","into","is","isn't","it","it's","its","just","least","let","like","likely","may","me","door"
	   ,"might","come","bounce","most","must","must've","mustn't","my","neither","no","nor","not","of","stuff"
	   ,"off","often","on","only","or","other","our","own","rather","said","say","says","sad","she","she'd","drive"
	   ,"she'll","she's","should","should've","shouldn't","since","so","some","than","that","that'll","that's"
	   ,"the","their","them","then","there","there's","these","they","they'd","they'll","they're","they've","store"
	   ,"this","yummy","to","too","twas","us","wants","was","wasn't","we","we'd","we'll","we're","were","shell"
	   ,"weren't","what","what'd","what's","when","when","when'd","muddy","when's","where","where'd","broken"
	   ,"happy","where's","which","while","who","who'd","who'll","who's","whom","would","wonder","what","fixed"
	   ,"you","cow","dog","cat","eat","ate","mine","yours","mother","funny","hairy","man","woman","why","why'd"
	   ,"why'll","why's","will","with","won't","would","would've","wouldn't","yet","you","you'd","you'll","slamed"
	   ,"you're","you've","your","hurt","hungry","food","beans","tree","face","wooden","stick","cup","bowl","hello"
	   ,"work","home","purchase","make","find","carry","heavy","light","burn","smash","banana","arm","leg","face"
	   ,"Frank","Cameron","Brad","Timmy","tacos","corn","bread","make","create","door","sand","paint","brain","clap"]

; Autohotkey has a limit on how long an expression can be so we break it up into two parts.
words.Push(["man","woman","birds","pizza","sheep","pig","goes","moo","pin","belong","kinship","laboratory","password"
       ,"fear","forecast","boy","field","screen","chart","tray","chemistry","rich","root","soup","guideline","flat"
	   ,"variation","health","comfortable","power","pardon","skilled","concert","mood","double","inhibition"
	   ,"identification","method","studio","chance","float","pack","contraction","mystery","plaintiff","throne"
	   ,"different","crystal","ignorant","chaos","bloodshed","will","traction","liberal","swipe","tract","authorise"
	   ,"establish","behave","matrix","island","palace","blue","contrary","divorce","stunning","slide","look"
	   ,"scandal","quest","domination","pneumonia","destruction","economist","committee","extend","pressure"
	   ,"veteran","breeze","indirect","acute","win","wriggle","interventions","overwhelm","press","outside"
	   ,"nomination","pan","fleet","remedy","bin","platform","fragrant","split","vehicle","dictate","communication"
	   ,"attitude","translate","auction","weapon","ideal","courtesy","present","scan","recording","breed","dogs"
	   ,"cows","captain","lost","citizen","fuss","kit","voter","session","bowel","musical","certain","file","first"
	   ,"heaven","insistence","nonremittal","bait","philosophy","morsel","station","printer","gradual","responsibility"
	   ,"acquaintance","diet","courtship","claim","intention","nervous","glass","eating","normal","close","calf","men"
	   ,"integrated","period","pigs","autonomy","large","school","training","venture","check","explosion","interrupt"
	   ,"bitter","intervention","rocket","gap","fan","veil","murder","message","run","collapse","ideal","communication"
	   ,"boat","harvest","loan","rib","put","bear","reflection","reasonable","complete","housewife","detective"
	   ,"supplementary","recordings","muscle","constituency","sulphur","lamb","flex","gold","plan","grudge","discuss"
	   ,"cell","phone","house","civilian","lick","list","duke","gossip","wedding","distort","burning","edge","obese"
	   ,"scatter","bomb","onion","compose","blame","interface","bird","hardship","recycle"])

	Random, looplength, 6, 25
	Loop % looplength
	{
		Random, word, 0, % words.Length()-1
		SendInput, % words[word]
		if (A_Index != looplength) {
			SendInput, {Space}
		} else {
			Punc := ["!", "?", ".", ","]
			Random, P, 1, % Punc.MaxIndex()
			SendInput, % Punc[P]
			SendInput, {Space}
		}
	}
return


;<----------------------------------------------------------------------------->
;<                     Define Left Column Shortcut   (Ctrl+d)                  >
;<----------------------------------------------------------------------------->


#IfWinActive, Microvellum Workbook Designer
^d::
	Send, !i
	Sleep, 100
	SendInput, {Down 3}
	Sleep, 100
	SendInput, {Right}
	Sleep, 100
	SendInput, {Down 3}
	Sleep, 100
	SendInput, {Enter}
	Sleep, 100
	SendInput, {Enter}
return
#IfWinActive





