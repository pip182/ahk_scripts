;<---------------------------------------------------------------------------->
;<                            Debug Console                                   >
;<---------------------------------------------------------------------------->


DebugMessage(str)
{
 global h_stdout
 DebugConsoleInitialize()  ; start console window if not yet started
 str .= "`n" ; add line feed
 DllCall("WriteFile", "uint", h_Stdout, "uint", &str, "uint", StrLen(str), "uint*", BytesWritten, "uint", NULL) ; write into the console
 WinSet, Bottom,, ahk_id %h_stout%  ; keep console on bottom
}

DebugConsoleInitialize()
{
   global h_Stdout     ; Handle for console
   static is_open = 0  ; toogle whether opened before
   if (is_open = 1)     ; yes, so don't open again
     return

   is_open := 1
   ; two calls to open, no error check (it's debug, so you know what you are doing)
   DllCall("AttachConsole", int, -1, int)
   DllCall("AllocConsole", int)

   dllcall("SetConsoleTitle", "str","Paddy Debug Console")    ; Set the name. Example. Probably could use a_scriptname here
   h_Stdout := DllCall("GetStdHandle", "int", -11) ; get the handle
   WinSet, Bottom,, ahk_id %h_stout%      ; make sure it's on the bottom
   WinActivate,Lightroom   ; Application specific; I need to make sure this application is running in the foreground. YMMV'
   return
}

;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                  Get Selected Text Function                                              >
;<-------------------------------------------------------------------------------------------------------------------------->

;   EXAMPLE OF USE:
/*
 * F1::
 *
 * SelectedText := gst()
 *
 * MsgBox,,, % SelectedText,1	; you can use it as a variable within the script
 *
 * return
 */

gst() {
	IsClipEmpty := (Clipboard = "") ? 1 : 0
	if !IsClipEmpty {
		ClipboardBack50 := ClipboardAll
		While !(Clipboard = "") {
			Clipboard =
			Sleep, 10
		}
	}
	Send, ^c
	ClipWait, 0.1
	Clipboard := RegexReplace( Clipboard, "^\s+|\s+$" )
	;Clipboard := %Clipboard%
	roReturn := Clipboard, Clipboard := ClipboardBack50
	if !IsClipEmpty
		ClipWait, 0.5, 1
	return roReturn
}

;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                   Zero Trimmer Function                                                  >
;<-------------------------------------------------------------------------------------------------------------------------->

trimmer(num)
{
	If num IS space
	return num
	If num IS NOT float
	return num
	  Loop, % StrLen(num)
	  {
		StringRight the_number, num, 1
		If (the_number = ".")
		 {
		  StringTrimRight, num, num, 1
		Break
		 }
		If (the_number = "0")
		  StringTrimRight, num, num, 1
	  }
	If num IS NOT number
	return 0
	return num
}

;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                       Calc command                                                       >
;<-------------------------------------------------------------------------------------------------------------------------->

calc(how, num, amount)
{
	out := num
	if (how = 0) {
		out += amount
	}
	else if (how = 1) {
		out -= amount
	}
	else if (how = 2){
		out /= amount
	}
	else if (how = 3) {
		out *= amount
	}
	return out
}

;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                 Do Cool Things With Selected Text										>
;<-------------------------------------------------------------------------------------------------------------------------->
;
; DONE: Add ability to roll mouse wheel to increase / decrease by whole number
; TODO: If there are CRs in the selected text, add a new line after pasting number. (Really depends on how the text was selected to begin with)

~LWin & WheelUp::
{
	SelectedText := gst()
	If SelectedText is not number
		Exit
	SelectedText := trimmer(SelectedText) + 1.0
	SendInput % trimmer(SelectedText)
	;DebugMessage(trimmer(SelectedText))
	SendInput, {Shift Down}
	Elf := trimmer(SelectedText)
  	Loop, Parse, Elf
  	{
  		SendInput, {left}
  	}
  	SendInput, {Shift Up}
	Sleep,50
	return
}

~LWin & WheelDown::
{
	SelectedText := gst()
	If SelectedText is not number
		Exit
	SelectedText := trimmer(SelectedText) - 1.0
	Elf := trimmer(SelectedText)
	SendInput, % trimmer(SelectedText)
	;DebugMessage(trimmer(SelectedText))
	SendInput, {Shift Down}
  	Loop, Parse, Elf
  	{
  		SendInput, {left}
  	}
  	SendInput, {Shift Up}
	Sleep,50
	return
}

~LWin & LButton::
{
	IfWinActive, Lazy Calc
	{
		Gosub 14GuiClose
	}
	SelectedText := gst()
	Width := 370
	If SelectedText is not number
		return
	CoordMode, mouse, Screen
	MouseGetPos, mx, my
	Gui, 14:Font,s15 c0000FF bold, Verdana
	Gui, 14:Add, Text, % "x0 y05 w" . Width . " h25 vTX +Center", %SelectedText%
	Gui, 14:Font,s10 bold, Verdana
	Gui, 14:Add, Button, % "x15 y40 w60 h25 +Disabled vBAdd gB_Add", +
	Gui, 14:Add, Button, % "x+10 y40 w60 h25 vBSub gB_Sub", -
	Gui, 14:Add, Button, % "x+10 y40 w60 h25 vBDiv gB_Div", /
	Gui, 14:Add, Button, % "x+10 y40 w60 h25 vBMult gB_Mult", x
	Gui, 14:Add, Button, % "x+10 y40 w60 h25 gB_CLR", CLR
	Gui, 14:Font,s09 c000000 norm, Verdana

	Gui, 14:Add, Button, % "x15 y70 w60 h25 gB_1_32", 0.03125
	Gui, 14:Add, Button, % "x+10 y70 w60 h25 gB_1_16", 0.0625
	Gui, 14:Add, Button, % "x+10 y70 w60 h25 gB_1_8", 0.125
	Gui, 14:Add, Button, % "x+10 y70 w60 h25 gB_1_4", 0.25
	Gui, 14:Add, Button, % "x+10 y70 w60 h25 gB_3_8", 0.375
	Gui, 14:Add, Button, % "x15 y100 w60 h25 gB_7_16", 0.4375
	Gui, 14:Add, Button, % "x+10 y100 w60 h25 gB_1_2", 0.5
	Gui, 14:Add, Button, % "x+10 y100 w60 h25 gB_5_8", 0.625
	Gui, 14:Add, Button, % "x+10 y100 w60 h25 gB_3_4", 0.75
	Gui, 14:Add, Button, % "x+10 y100 w60 h25 gB_7_8", 0.875
	Gui, 14:Add, Button, % "x15 y130 w60 h25 gB_1", 1
	Gui, 14:Add, Button, % "x+10 y130 w60 h25 gB_2", 2
	Gui, 14:Add, Button, % "x+10 y130 w60 h25 gB_3", 3
	Gui, 14:Add, Button, % "x+10 y130 w60 h25 gB_4", 4
	Gui, 14:Add, Button, % "x+10 y130 w60 h25 gB_5", 5
	Gui, 14:Add, Button, % "x15 y160 w60 h25 gB_6", 6
	Gui, 14:Add, Button, % "x+10 y160 w60 h25 gB_7", 7
	Gui, 14:Add, Button, % "x+10 y160 w60 h25 gB_8", 8
	Gui, 14:Add, Button, % "x+10 y160 w60 h25 gB_9", 9
	Gui, 14:Add, Button, % "x+10 y160 w60 h25 gB_10", 10

	W := Width-30
	Gui, 14:Add, Button, % "x15 y190 w" . W . " h45 gB_OK", Input
	Gui, 14:+AlwaysOnTop
	Gui, 14:+Owner -0x30000
	;Gui, 14:+ToolWindow

	output := trimmer(SelectedText)
	; Actions are as follows:
	; 0 : Add
	; 1 : Subract
	; 2 : Divide
	; 3 : Multiply
	action := 0

	Gui, 14:Show, x%mx% y%my% w%Width% h250, Lazy Calc
	return
}

	14UpdateDisplay:
	{
		GuiControl,,TX, % trimmer(OutPut)
		return
	}

	14GuiClose:
	14GuiEscape:
	{
		Send, !{Esc}
		SendInput % trimmer(Output)
		Gui, 14:Destroy
		;ExitApp
		Exit
	}

	B_OK:
		GoSub 14GuiClose
	return

	B_Add:
		action:=0
		GuiControl,+Disabled,BAdd,
		GuiControl,-Disabled,BSub
		GuiControl,-Disabled,BMult
		GuiControl,-Disabled,BDiv
	return

	B_Sub:
		action:=1
		GuiControl,-Disabled,BAdd
		GuiControl,+Disabled,BSub
		GuiControl,-Disabled,BMult
		GuiControl,-Disabled,BDiv
	return

	B_Div:
		action:=2
		GuiControl,-Disabled,BAdd
		GuiControl,-Disabled,BSub
		GuiControl,-Disabled,BMult
		GuiControl,+Disabled,BDiv
	return

	B_Mult:
		action:=3
		GuiControl,-Disabled,BAdd
		GuiControl,-Disabled,BSub
		GuiControl,+Disabled,BMult
		GuiControl,-Disabled,BDiv
	return

	B_CLR:
		Output := 0
		Gosub 14UpdateDisplay
	return

	B_1_32:
		Output := calc(action,Output,0.03125)
		Gosub 14UpdateDisplay
	return

	B_1_16:
		Output := calc(action,Output,0.0625)
		Gosub 14UpdateDisplay
	return

	B_1_8:
		Output := calc(action,Output,0.125)
		Gosub 14UpdateDisplay
	return

	B_1_4:
		Output := calc(action,Output,0.25)
		Gosub 14UpdateDisplay
	return

	B_3_8:
		Output := calc(action,Output,0.375)
		Gosub 14UpdateDisplay
	return

	B_7_16:
		Output := calc(action,Output,0.4375)
		Gosub 14UpdateDisplay
	return

	B_1_2:
		Output := calc(action,Output,0.5)
		Gosub 14UpdateDisplay
	return

	B_5_8:
		Output := calc(action,Output,0.625)
		Gosub 14UpdateDisplay
	return

	B_3_4:
		Output := calc(action,Output,0.75)
		Gosub 14UpdateDisplay
	return

	B_7_8:
		Output := calc(action,Output,0.875)
		Gosub 14UpdateDisplay
	return

	B_1:
		Output := calc(action,Output,1)
		Gosub 14UpdateDisplay
	return

	B_2:
		Output := calc(action,Output,2)
		Gosub 14UpdateDisplay
	return

	B_3:
		Output := calc(action,Output,3)
		Gosub 14UpdateDisplay
	return

	B_4:
		Output := calc(action,Output,4)
		Gosub 14UpdateDisplay
	return

	B_5:
		Output := calc(action,Output,5)
		Gosub 14UpdateDisplay
	return

	B_6:
		Output := calc(action,Output,6)
		Gosub 14UpdateDisplay
	return

	B_7:
		Output := calc(action,Output,7)
		Gosub 14UpdateDisplay
	return

	B_8:
		Output := calc(action,Output,8)
		Gosub 14UpdateDisplay
	return

	B_9:
		Output := calc(action,Output,9)
		Gosub 14UpdateDisplay
	return

	B_10:
		Output := calc(action,Output,10)
		Gosub 14UpdateDisplay
	return


~Lwin & RButton::
		Send, {Enter}
return
