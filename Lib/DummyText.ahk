/*
Name: Phio - Lipsum Generator
Version 1.1 (Fri April 10, 2015)
Created: Thu April 09, 2015
Author: tidbit
Credit:
	- Emmet and EverEdit for basic concepts/ideas
	- Animals http://a-z-animals.com/animals/
	- Veggies http://idsgn.dropmark.com/107/1116

Hotkeys:

Description:
	Generate Lorem Ipsum words, sentences and paragraphs. Handy placeholder text
	for when you don't have actual content to fill in your program/site/other.

*/
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance, force

_name_=Phio - Lipsum Generator
_version_=1.1 (Fri April 10, 2015)
_description_=
(join
%_name_%
`nVersion: %_version_%
`nCreated: Thu April 09, 2015
`nAuthor: tidbit
`nCredit:
`n`t- Emmet and EverEdit for basic concepts/code samples
`n`t- Animals http://a-z-animals.com/animals/
`n`t- Veggies http://idsgn.dropmark.com/107/1116

`n`nDescription:
`n`tGenerate Lorem Ipsum words, sentences, lists and paragraphs. Handy placeholder
text for when you don't have actual content to fill in your program or website.
`nIncludes a few databases for words other than the standard Latin words.

`n`nLorem ipsum is used due to how it looks. Repeating the same word(s) is very
 noticable to the human eye. This gives random length words with commas and
 punctuation to make an illusion of real content, not just a repeated word.
)

databases:="Lorem||Animals|Veggies|Pirate"

; call these functions by themselves (see their parameters below):
; msgBox % tostr(paragraph())
; msgBox % tostr(sentence())
; msgBox % tostr(words())


menu, mbar, add, About, About
gui, menu, mbar

gui, margin, 6, 6
gui, font, s10
gui, add, radio, xm ym vmode1 gsettings, Words
gui, add, radio, checked vmode2 gsettings, Sentences/List
gui, add, radio, vmode3 gsettings, Paragraph

gui, add, text, cgreen, List:
gui, add, dropDownList,x+m yp w100 cgreen hwndhDDL vdatabase, %databases%
SendMessage, 0x0160, 150, 0, , ahk_id %hDDL%

gui, add, checkbox, xm cred checked vbegin, Begin with`n"Lorem ipsum..."?

gui, add, text, 0x5 wp h1 section vline1, ; horiz
gui, add, text, xp yp 0x5 w1 r14 vline2, ; vert

gui, add, text, xs+8 ys+8 section cblue vtxt1, Min words:
gui, add, text, cblue vtxt2, Max words:
gui, add, text, cblue vtxt3, Paragraphs:

gui, add, edit, ys w50 number vminWord, 3
gui, add, edit, wp number vmaxWord, 12
gui, add, edit, wp number vparaCount, 3

gui, add, button, xs default vgenBtn gdoIt, Generate!
gui, add, button, xs default vCopyBtn gCopyIt, Copy To Clipboard!

gui, font, s10
; gui, font
gui, add, edit, ym w400 r18 vdisp

; pointless stuff for a pointless effect
guiControlGet, pos1, pos, paraCount
guiControlGet, pos2, pos, line1
guiControl, move, line1, % "w" pos1x+pos1w-pos2x+5
guiControlGet, pos1, pos, line1
guiControlGet, pos2, pos, copyBtn
guiControl, move, genBtn, % "w" pos1w-pos1x-6
guiControl, move, copyBtn, % "w" pos1w-pos1x-6
guiControl, move, line2, % "h" pos2y-pos1y+pos2h
guiControl, move, disp, % "h" pos2y+pos2h-6

gosub, doIt
gosub, settings

gui, show, autoSize, Dummy Text Generator (For dum dums)

OnMessage(WM_MOUSEWHEEL:=0x20A, "wheel")
OnMessage(WM_MOUSEMOVE:=0x200, "drag")
return


guiClose:
	exitapp
return


about:
	msgBox, 64, About %_name_%, %_description_%
return

copyIt:
  guiControlGet, txt,, disp
  clipboard = %txt%
return

doIt:
	gui, submit, nohide
	mode:=(mode3=1) ? 3 : (mode2=1) ? 2 : 1

	if (mode=1)
		maxWord:=minWord
	else
	{
		guiControl,, minWord, % (maxWord<minWord) ? maxWord : minWord
		guiControl,, maxWord, % (maxWord>minWord) ? maxWord : minWord
	}

	out:=li(mode, minWord, maxWord, paraCount, begin, database)
	guicontrol,, disp, %out%
return


settings:
	gui, submit, nohide
	mode:=(mode3=1) ? 3 : (mode2=1) ? 2 : 1
	if (mode=1)
	{
		guiControl,, txt1, Words
		guiControl, Disable, maxWord
		guiControl, Disable, paraCount
		guiControl, Disable, begin
	}
	if (mode=2)
	{
		guiControl,, txt1, Min words
		guiControl,, txt3, Lines
		guiControl, Enable, maxWord
		guiControl, Enable, paraCount
		guiControl, Disable, begin
	}
	if (mode=3)
	{
		guiControl,, txt1, Min words
		guiControl,, txt3, Paragraphs
		guiControl, Enable, maxWord
		guiControl, Enable, paraCount
		guiControl, Enable, begin
	}
return


; MAIN FUNCTION
; mode = 1=word, 2=sentence, 3=paragraph
; senMin = minimum words per sentence OR used in mode 1, the number of words to gen
; senMax = max words per sentence
; paraCount = how many paragraphs to generate
; useCommon = start with Lorem ipsum dolor......
; the amount of sentences per paragraph is 3-9
; returns a string
li(mode=1, senMin=3, senMax=12, paraCount=1, useCommon=1, database="Lorem")
{
	temp:=senMin
	senMin:=(senMax<senMin) ? senMax : senMin
	senMax:=(temp>senMax) ? temp : senMax
	list:=""
	if (mode=1)
		return tostr(words(senMin, database))
	if (mode=2)
	{
		loop, %paraCount%
			list.=tostr(sentence(senMin, senMax, database)) "`n`n"
	}
	if (mode=3)
	{
		; list.=paragraph(senMin, senMax, paraCount, useCommon, database)
		list.=tostr(paragraph(senMin, senMax, paraCount, useCommon, database))
	}
	return trim(list, "`r`n `t")
}


toTitle(in)
{
	stringUpper, in, in, T
	return in
}

; thanks Emmet / EverEdit!
choose(chars*)
{
	return chars[rand(chars.maxIndex())]
}

tostr(arr, delim=" ")
{
	for k,v in arr
		out.=v . ((v~="[\r\n]") ? "" : delim)
	return rTrim(out, delim "`r`n `t")
}

rand(max=1, min=1)
{
	temp:=min
	min:=(max<min) ? max : min
	max:=(temp>max) ? temp : max
	random, out, %min%, %max%
	return, out
}

; thanks Emmet / EverEdit!
addCommas(arr)
{
	len:=arr.maxIndex()
	if (len>3 && len<=6)
		totalCommas:=rand(0, 1)
	else if (len>6 && len<=12)
		totalCommas:=rand(0, 2)
	else if (len>12)
		totalCommas:=rand(1, 4)
	else
		totalCommas:=0

	while (totalCommas>0)
	{
		temp:=rand(len-1)
		if (!InStr(arr[temp], ","))
			arr[temp].=","
			, totalCommas-=1
	}
	return arr
}


; returns an array
; min      = minimum sentence length.
; max      = maximum sentence length.
; database = what database to pull the words from.
; depends on: rand, words, choose, toTitle and addCommas.
sentence(min=3, max=12, database="lorem")
{
	temp:=rand(min,max) ; words per sentence
	line:=words(temp, database)
	line[temp].=choose(".",".",".","?","!")
	line[1]:=toTitle(line[1])
	line:=addCommas(line)
	return line
}


; returns an array
; min       = minimum sentence length. NOT paragraph word count!
; max       = maximum sentence length. NOT paragraph word count!
; paraCount = how many paragraphs to generate.
; useCommon = use the standard "Lorem ipsum" sentence?
; database  = what database to pull the words from.
; paragraphs are a fixed length of 3-9 sentences.
; depends on: rand, words, sentence, choose, toTitle and addCommas.
paragraph(min=3, max=12, paraCount=1, useCommon=1, database="Lorem")
{
	common:=""
	commonArr:=strsplit("Lorem ipsum dolor sit amet, consectetur adipisicing elit.", " ")
	out:=[]
	loop, %paraCount% ; number of paragraphs
	{
		line:=[]
		senCount:=rand(3,9)
		loop, %senCount%  ; number of sentences
		{
			temp:=rand(min,max) ; words per sentence
			if (useCommon=1)
			{
				; extract N amount of words from the template line
				for k, v in commonArr
					if (a_index<=temp)
						line.push(v)

				; if the cutoff was before the last word in the template,
				; add a random punctuation
				if (temp<commonArr.maxIndex())
				{
					; get the last letter. if it's a symbol, remove it so we can
					; have a generated punctuation mark
					last:=subStr(line[temp], 0)
					if last is not alpha
						line[temp]:=regExReplace(line[temp], "[!?., ]+$")

					line[temp].=choose(".",".",".","?","!")
				}

				out.push(line*)
				; list.=tostr(line) " "
				useCommon:=0 ; don't use it anymore for this session.
			}
			else
			{
				out.push(sentence(min, max, database)*)
				; list.=tostr(sentence(min, max, database)) " "
			}
		}
		out.push("`n`n")
		; list:=rtrim(list)
		; list.="`n`n"
	}
	return out
	; return list
}

; returns an array
; total    = how many words to grab
; database = what set of words to use
; depends on: rand
words(total=3, database="lorem")
{
	; thanks Emmet / EverEdit!
	lorem:=["a","ab","accusamus","accusantium","ad","adipisci","alias","aliquam","aliquid","amet","animi","aperiam","architecto","asperiores","aspernatur","assumenda","at","atque","aut","autem","beatae","blanditiis","cam","camque","commodi","consectetur","consequatur","consequuntur","corporis","corrupti","culpa","cupiditate","debitis","delectus","deleniti","deserunt","dicta","dignissimos"
	,"distinctio","dolor","dolore","dolorem","doloremque","dolores","doloribus","dolorum","ducimus","ea","eaque","earum","eius","eligendi","enim","eos","error","esse","est","et","eum","eveniet","ex","excepturi","exercitationem","expedita","explicabo","facere","facilis","fuga","fugiat","fugit","harum","hic","id","illo","illum","impedit","in","incidunt","inventore","ipsa","ipsam","ipsum","iste"
	,"itaque","iure","iusto","labore","laboriosam","laborum","laudantium","libero","magnam","magni","maiores","maxime","minima","minus","modi","molestiae","molestias","mollitia","nam","natus","necessitatibus","nemo","neque","nesciunt","nihil","nisi","nobis","non","nostrum","nulla","numquam","obcaecati","odio","odit","officia","officiis","omnis","optio","pariatur","perferendis","perspiciatis"
	,"placeat","porro","possimus","praesentium","provident","quae","quaerat","quam","quas","quasi","qui","quia","quibusdam","quidem","quis","quisquam","quo","quod","quos","ratione","recusandae","reiciendis","rem","repellat","repellendus","reprehenderit","repudiandae","rerum","saepe","sapiente","sed","sequi","similique","sint","sit","soluta","sunt","suscipit","tempora","tempore","temporibus"
	,"tenetur","totam","ullam","unde","ut","vel","velit","veniam","veritatis","vero","vitae","voluptas","voluptate","voluptatem","voluptates","voluptatibus","voluptatum"]

	animals:=["akita","albatross","alligator","angelfish","ant","anteater","antelope","armadillo","axolotl","baboon","badger","balinese","bandicoot","barracuda","bat","bear","beaver","beetle","bird","birman","bison","bloodhound","bobcat","booby","buffalo","bulldog","bullfrog","burmese","butterfly","caiman","camel","capybara","caracal","cassowary","cat","caterpillar","catfish","centipede","chameleon"
	,"chamois","cheetah","chicken","chimpanzee","chinchilla","chinook","chipmunk","cichlid","coati","cockroach","collie","coral","cougar","cow","coyote","crab","crane","crocodile","cuscus","cuttlefish","dachshund","deer","dingo","dodo","dog","dolphin","donkey","dormouse","dragonfly","drever","duck","dugong","dunker","eagle","earwig","echidna","elephant","emu","falcon","ferret","fish","flamingo"
	,"flounder","fly","fossa","fox","frigatebird","frog","gar","gecko","gerbil","gibbon","giraffe","goat","goose","gopher","gorilla","grasshopper","greyhound","grouse","guppy","hamster","hare","harrier","havanese","hedgehog","heron","himalayan","hippopotamus","horse","hummingbird","hyena","ibis","impala","indri","insect","jackal","jaguar","javanese","jellyfish","kakapo","kangaroo","kingfisher"
	,"kiwi","koala","kudu","labradoodle","ladybird","lemming","lemur","leopard","liger","lion","lionfish","lizard","llama","lobster","lynx","macaw","magpie","manatee","mandrill","markhor","mastiff","meerkat","millipede","mole","molly","mongoose","mongrel","monkey","moose","moth","mouse","mule","newfoundland","newt","nightingale","numbat","ocelot","octopus","okapi","olm","opossum","ostrich"
	,"otter","oyster","panther","parrot","peacock","pelican","penguin","persian","pheasant","pig","pika","pike","piranha","platypus","pointer","poodle","porcupine","possum","prawn","puffin","pug","puma","quail","quetzal","quokka","quoll","rabbit","raccoon","ragdoll","rat","rattlesnake","reindeer","rhinoceros","rottweiler","salamander","scorpion","seal","serval","sheep","siberian","skunk"
	,"sloth","snail","snake","snowshoe","sparrow","squid","squirrel","stoat","tapir","tarsier","tetra","tiger","tortoise","toucan","tropicbird","tuatara","turkey","uakari","uguisu","umbrellabird","vulture","wallaby","walrus","warthog","wasp","weasel","whippet","wildebeest","wolf","wolverine","wombat","woodpecker","yak","zebra"]

	Veggies:=["sea","pea","napa","leek","kale","plum","gram","bean","nori","fava","bell","rock","root","okra","taro","corn","seed","soko","bush","beet","nuts","melon","maize","lotus","leone","chard","water","kombu","sweet","azuki","gumbo","shoot","bunya","swiss","green","grape","gourd","onion","dulse","desert","tomato","raisin","endive","tatsoi","radish","garlic","daikon","bamboo","bologi","greens"
	,"potato","sierra","silver","jícama","kakadu","wakame","pepper","peanut","sorrel","summer","turnip","spring","wattle","celery","sprout","squash","fennel","winter","yarrow","carrot","lentil","mustard","parsley","parsnip","lettuce","arugula","avocado","burdock","cabbage","catsear","prairie","pumpkin","sprouts","celtuce","spinach","soybean","chicory","collard","shallot","seakale","salsify","rutabaga"
	,"cucumber","scallion","zucchini","kohlrabi","earthnut","ricebean","eggplant","tigernut","chickpea","chestnut","garbanzo","broccoli","beetroot","quandong","brussels","purslane","plantain","amaranth","groundnut","courgette","radicchio","artichoke","chickweed","komatsuna","coriander","dandelion","aubergine","asparagus","bitterleaf","watercress","horseradish","cauliflower"]

	Pirate:=["deck","sail","sails","beard","patch","parrot","cannon","sword","scimitar","rum","her","barrel","scurvy","sea","monster","treasure","gold","arrrgh","arrrrr","matey","mate","captain","chest","island","poopdeck","plank","planks","gunpowder","port","harbor","fight","flee","locker","davey","One-Eyed Willy","crew","bunks","curse","legend","smuggle","dispatch","embark","travel"
	,"vessel","storage","craft","cannon-ball","crate","barge","bucket","rope","crows-nest","head","keel","bow","shaft","rudder","mast","Kraken","mermaid","coast","wreck","sunken","sink","Flying Dutchmen","code","kill","death","devil","brothel","myth","booty","raft","boom","explosion","she","be","thar","tharrr","har","ahoy","avast","wit'","ongoin'","runnin'","'til","ye","be","knowin'","off","shipment"
	,"pirate","pirates","raid","ambush","scalawag","Jolly Roger","strike","drink","drinkin'","drunk","washed-up","ashore","food","bread","bar","pub","north","south","east","west","stars","direction","bottle"]

	globalWords:=["much","many","very","with","those","other","to","a","together","of"]
	Veggies.push(globalWords*)
	Animals.push(globalWords*)
	Pirate.push(globalWords*)

	arr:=[]
	count:=1
	while (count<=total)
	{
		chosen:=%database%[rand(1, %database%.maxIndex()-1)]
		if (chosen=prev) ; prevent duplicate words in a row.
			continue
		arr[count]:=chosen
		prev:=chosen
		count+=1
	}
	return arr
}


drag(wParam, lParam)
{
   static yp,lastControl,change
   sensitivity:=7 ; In pixels, about how far should the mouse move before adjusting the value?
   amt:=1         ; How much to increase the value
   mode:=1        ; 1 = up/down, 2=left/right

   ; some safety checks
   if (!GetKeyState("Lbutton", "P"))
      return
   GuiControlGet, controlType, Focus
   if (!instr(controlType, "Edit"))
      return
   GuiControlGet, value,, %A_GuiControl%
   if value is not number
      return

   if (mode=1)
      MouseGetPos,, y
   else if (mode=2)
   {
      MouseGetPos, y
      y*=-1 ; need to swap it so dragging to the right adds, not subtracts.
   }
   else
      return

   if (lastControl!=A_GuiControl) ; set the position to the current mouse position
      yp:=y
   change:=abs(y-yp)              ; check to see if the value is ready to be changed, has it met the sensitivity?

   value+=((y<yp && change>=sensitivity) ? 1 : (y>yp && change>=sensitivity) ? -1 : 0)

   GuiControl,, %A_GuiControl%, % RegExReplace(value, "(\.[1-9]+)0+$", "$1")

   if (change>=sensitivity)
      yp:=y
   lastControl:=A_GuiControl
}
wheel(wParam, lParam)
{
   amt:=1 ; How much to increase the value
   GuiControlGet, controlType, Focus
   if (!instr(controlType, "Edit"))
      return

   GuiControlGet, value,, %A_GuiControl%
   if value is not number
      return
   value+=((StrLen(wParam)>7) ? -1 : 1)
   GuiControl,, %A_GuiControl%, % RegExReplace(value, "(\.[1-9]+)0+$", "$1")
}
