
#SingleInstance
; Example: Simple text editor with menu bar.


; Create the sub-menus for the menu bar:
	Menu, FileMenu, Add, &New, FileNew
	Menu, FileMenu, Add, &Open, FileOpen
	Menu, FileMenu, Add, &Save, FileSave
	Menu, FileMenu, Add, Save &As, FileSaveAs
	Menu, FileMenu, Add  ; Separator line.
	Menu, FileMenu, Add, E&xit, FileExit

	; Create the menu bar by attaching the sub-menus to it:
	Menu, MyMenuBar, Add, &File, :FileMenu

	; Attach the menu bar to the window:
	Gui, Menu, MyMenuBar

	; Create the main Edit control and display the window:
	Gui, Font, s18 tahoma
	Gui, +Resize  ; Make the window resizable.
	Gui, Add, Edit, vMainEdit WantTab W500 R15
	Gui, Add, Button, bDone x170 w200, Done
	Gui, 14:+AlwaysOnTop
	Gui, 14:+Owner -0x30000
	Gui, Show, W525 H550, Note Editor
	CurrentFileName =  ; Indicate that there is no current file.
return

FileNew:
GuiControl,, MainEdit  ; Clear the Edit control.
return

FileOpen:
Gui +OwnDialogs  ; Force the user to dismiss the FileSelectFile dialog before returning to the main window.
FileSelectFile, SelectedFileName, 3,, Open File, Text Documents (*.txt)
if SelectedFileName =  ; No file selected.
    return
Gosub FileRead
return

FileRead:  ; Caller has set the variable SelectedFileName for us.
FileRead, MainEdit, %SelectedFileName%  ; Read the file�s contents into the variable.
if ErrorLevel
{
    MsgBox Could not open �%SelectedFileName%�.
    return
}
GuiControl,, MainEdit, %MainEdit%  ; Put the text into the control.
CurrentFileName = %SelectedFileName%
Gui, Show,, %CurrentFileName%   ; Show file name in title bar.
return

FileSave:
if CurrentFileName =   ; No filename selected yet, so do Save-As instead.
    Goto FileSaveAs
Gosub SaveCurrentFile
return

FileSaveAs:
Gui +OwnDialogs  ; Force the user to dismiss the FileSelectFile dialog before returning to the main window.
FileSelectFile, SelectedFileName, S16,, Save File, Text Documents (*.txt)
if SelectedFileName =  ; No file selected.
    return
CurrentFileName = %SelectedFileName%
Gosub SaveCurrentFile
return

SaveCurrentFile:  ; Caller has ensured that CurrentFileName is not blank.
IfExist %CurrentFileName%
{
    FileDelete %CurrentFileName%
    if ErrorLevel
    {
        MsgBox The attempt to overwrite �%CurrentFileName%� failed.
        return
    }
}
GuiControlGet, MainEdit  ; Retrieve the contents of the Edit control.
FileAppend, %MainEdit%, %CurrentFileName%  ; Save the contents to the file.
; Upon success, Show file name in title bar (in case we were called by FileSaveAs):
Gui, Show,, %CurrentFileName%
return

GuiDropFiles:  ; Support drag & drop.
Loop, Parse, A_GuiEvent, `n
{
    SelectedFileName = %A_LoopField%  ; Get the first file only (in case there�s more than one).
    break
}
Gosub FileRead
return

GuiSize:
if ErrorLevel = 1  ; The window has been minimized.  No action needed.
    return
; Otherwise, the window has been resized or maximized. Resize the Edit control to match.
;NewWidth := A_GuiWidth � 20
;NewHeight := A_GuiHeight � 20
;GuiControl, Move, MainEdit, W%NewWidth% H%NewHeight%
return

FileExit:     ; User chose �Exit� from the File menu.
GuiClose:  ; User closed the window.
ExitApp