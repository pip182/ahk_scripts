GroupAdd IGNORE, ahk_class indesign
GroupAdd IGNORE, ahk_exe Code.exe

#IfWinNotActive ahk_exe Code.exe

~LAlt & LButton::
{
   IfWinExist, Note Editor
  {
    Gosub 15GuiClose
  }
    Gui, 15:Font, s14 tahoma
    SelectedText := gst()
    StringReplace, SelectedText, SelectedText,<br>, `n, All
    Gui, 15:+Resize  ; Make the window resizable.
    Gui, 15:Add, Edit, vMainEdit W500 R15
    Gui, 15:Font, s18 Tahoma
    Gui, 15:Add, Button, gDoneButt x170 w200, Done
    GuiControl, 15:, MainEdit, %SelectedText%
    Gui, 15:+AlwaysOnTop
    Gui, 15:+Owner -0x30000
    Gui, 15:Show, W525 H450, Note Editor
  return
}

15GuiClose:  ; User closed the window.
15GuiEscape:
{
  Gui, 15:Destroy
  Exit
}


DoneButt:
  ; Retrieve the contents of the Edit control.
  GuiControlGet, MainEdit
  ; Replace all new lines with a <br> tag
  StringReplace, OutputVar, MainEdit, `n,<br>, All
  Send, !{Esc}
  SendInput {Raw}%OutputVar%
  GoSub, 15GuiClose
return


#IfWinNotActive
