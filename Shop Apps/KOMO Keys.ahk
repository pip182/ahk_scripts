;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                      VIRTUAL NERD!!!                                                     >
;<-------------------------------------------------------------------------------------------------------------------------->



#SingleInstance Force
;#installKeybdHook
#Persistent

; KOMO helper script. Provides easy access to common forumlas and commands.

; Reference:
; Pop up message on top of all other windows.
;  MsgBox, 4096, Test

;Gui, Destroy

Width := 212
Guixpos := A_ScreenWidth - Width - 20
Gui, +LastFound
WinSet, Transparent, 225
Gui, Color, 808080
Gui, Margin, 0, 0
Gui, Font, s12 cFEFEFE Bold
Gui, Add, Progress, % "x-1 y-1 w" (Width+2) " h31 Background404040 Disabled hwndHPROG"
Gui, Add, Text, % "x0 y0 w" Width " h30 BackgroundTrans Center 0x200 gGuiMove vCaption", KOMO Kommander
Gui, Font, s10
Gui, Add, Text, % "x16 y+10 w" (Width-14) "r1 cWhite vTX5 gDropDrill", Drop Drill Bit (Ctrl+Win+1)
Gui, Add, Text, % "x16 y+10 w" (Width-14) "r1 cWhite vTX10 gToolChange", Tool Change (Ctrl+Win+2)
Gui, Add, Text, % "x16 y+10 w" (Width-14) "r1 cWhite vTX1 gEngageTool", Engage Tool (Ctrl+Win+3)
Gui, Add, Text, % "x16 y+10 w" (Width-14) "r1 cWhite vTX2 gRaiseHead", Raise Head (Ctrl+Win+4)
Gui, Add, Text, % "x16 y+10 w" (Width-14) "r1 cWhite vTX3 gReturnHome", Return Home (Ctrl+Win+5)
Gui, Add, Text, % "x16 y+10 w" (Width-14) "r1 cWhite vTX6 gRightSide", Right Side (Ctrl+Win+6)


;Gui, Add, Text, % "x26 y+10 w" (Width-14) "r1 cWhite vTX2 gPartLabels", Make Nested Part Labels (Ctrl+Win+4)
;Gui, Add, Text, % "x26 y+10 w" (Width-14) "r1 cWhite vTX2 gPartLabels", Make Nested Part Labels (Ctrl+Win+4)
Gui, Add, Text, % "x12 y+16 w" (Width-14) "r1 Center vTX4 Background707070 cFF8080 gClose", Close
Gui, Add, Text, % "x12 y+1 w" (Width-14) "h5 vP"

GuiControlGet, P, Pos
H := PY + PH
Gui, -Caption +AlwaysOnTop
WinSet, Region, 0-0 w%Width% h%H% r6-6
Gui, Show, % "y20 x" Guixpos "w" Width
return

GuiMove:
  PostMessage, 0xA1, 2
return


;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                       Drop Drill                                                         >
;<-------------------------------------------------------------------------------------------------------------------------->


^#1::
DropDrill:
{

    ;MsgBox, 4128, What do?, Make sure you have 1 red, 1 green and a few of the large white labels in the printer in that order.
    ;Send, !{Esc}
  InputBox, UserInput, Drill Bit, Which drill bit do you want to drop?
  Send, !{Esc}
  SendInput, {Raw}T30%UserInput%M3S0;

Return

}

;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                       Tool Change                                                        >
;<-------------------------------------------------------------------------------------------------------------------------->

^#2::
ToolChange:
{

  InputBox, UserInput, Tool Change, Which tool do you want to switch to?
  Send, !{Esc}
  SendInput, {Raw}T200%UserInput%M6;

Return
}


;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                      Engage Tool                                                         >
;<-------------------------------------------------------------------------------------------------------------------------->

^#3::
EngageTool:
{

  Send, !{Esc}
  SendInput,  {Raw}T102M3S0;

Return
}


;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                         Raise Head                                                       >
;<-------------------------------------------------------------------------------------------------------------------------->

^#4::
RaiseHead:
{

  Send, !{Esc}
  SendInput,  {Raw}G0G49Z0M5;

Return
}


;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                        Return Home                                                       >
;<-------------------------------------------------------------------------------------------------------------------------->

^#5::
ReturnHome:
{

  Send, !{Esc}
  SendInput, {Raw}G28G91X0Y0;

Return
}


;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                        Right Side                                                        >
;<-------------------------------------------------------------------------------------------------------------------------->

^#6::
RightSide:
{

  Send, !{Esc}
  SendInput, {Raw}G0G17G20G40G49G64G80G90Z0M5;
  SendInput, {Raw}N20G53X0Y-70.;

Return
}


;<-------------------------------------------------------------------------------------------------------------------------->
;<                                                      Close The App                                                       >
;<-------------------------------------------------------------------------------------------------------------------------->

Close:
{
  MsgBox, 4,Whoa..., Are you sure you want to close this?
  IfMsgBox Yes
  {
    Gui, Destroy
  }

  return
}
