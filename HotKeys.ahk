;PathToMVHelper := "O:\Departments\IT\HotKeys\MV_Helper.ahk"

#SingleInstance force


;<----------------------------------------------------------------------------->
;                           Re-Mapped Keys For Super Nerds
;<----------------------------------------------------------------------------->


#IfWinNotActive ahk_class PuTTYNG
^e::
  IfWinExist, Edit Formula
  {
    WinActivate
    Loop, 5{
      Send, {Tab}
    }
    SendInput {End}
  }
  else
  {
    SendInput {End}
  }
return


^a::
  IfWinExist, Edit Formula
  {
    WinActivate
    Loop, 5{
      Send, {Tab}
    }
    SendInput {Home}
  }
  else
  {
    SendInput {Home}
  }
return
#IfWinNotActive


;<----------------------------------------------------------------------------->
;                                  Microvellum Stuff
;<----------------------------------------------------------------------------->


; WMF Snapshot
^\::
  SetKeyDelay, 50
  SendEvent, {Esc} {Esc}
  SendEvent, select
  SendEvent, {Enter}
  Sleep, 100
  SendEvent, all
  SendEvent, {Enter}
  Sleep, 100
  SendEvent, {Enter}
  Sleep, 100
  SendEvent, change
  SendEvent, {Enter}
  Sleep, 100
  SendEvent, p
  SendEvent, {Enter}
  Sleep, 100
  SendEvent, c
  SendEvent, {Enter}
  Sleep, 100
  SendEvent, 255
  SendEvent, {Enter}
  Sleep, 100
  SendEvent, {Enter}
  Sleep, 100
  SendEvent, hide
  SendEvent, {Enter}
  Sleep, 500
  SendInput, mvmake
  SendInput, windows
  SendInput, metafile
  Sleep, 200
  SendEvent, {Enter}
  WinWait, Microvellum, Do you want to hide the lines?, 5
  WinActivate, Microvellum Do you want to hide the lines?
  SendEvent, {Enter}
return


; Run the WMF command again
#\::
  SetKeyDelay, 50
  SendEvent, hide {Enter}
  Sleep, 500
  SendInput, mvmake
  SendInput, windows
  SendInput, metafile
  Sleep, 200
  SendEvent, {Enter}
  WinWait, Microvellum, Do you want to hide the lines?, 5
  WinActivate, Microvellum Do you want to hide the lines?
  SendEvent, {Enter}
return


; WMF Set colors back to normal
!\::
  SetKeyDelay, 50
  SendEvent, select all {Enter}
  Sleep, 300
  SendEvent, change p c bylayer {Enter}
return


#IfWinActive, Copy File
Enter::
  CoordMode, Mouse, Client
  MouseClick, left, 30, 155
  Sleep, 300
  SendInput, {Enter}
return
#IfWinActive


#IfWinActive, User Access Denied. Enter Administrator Credentials...
Enter::
  Send, {Tab}
  Send, {Delete}Admin{Down}
  Send, {Tab}
  Send, {Delete}1admin2
  Sleep, 300
  MouseClick, left, 30, 105
  MouseClick, left, 30, 125

  ; Older MV version
  ;CoordMode, Mouse, Client
  ;MouseClick, left, 350, 18
  ;Send, {Delete}Admin Administrator
  ;MouseClick, left, 170, 39
  ;MouseClick, left, 125, 50
  ;Send, {Delete}1admin2
  ;MouseClick, left, 30, 85

return
#IfWinActive


#IfWinActive, ahk_class AfxMDIFrame90u
^w::
  SendRaw, close
  SendInput, {Enter}
return
#IfWinActive


#IfWinActive, ahk_exe toolbox.exe
; Call Line function.
^#l::
  SendInput pline
  SendInput {Enter}
return


;Call Offset function.
^#o::
  SendInput offset
  SendInput {Enter}
return


;Call Pedit function.
^#p::
  SendInput pedit
  SendInput {Enter}
  SendInput m
  SendInput {Enter}
return


;Call Linear Dimension function.
^#d::
  SendInput dimlinear
  SendInput {Enter}
return


;Call Copy function.
^#c::
  SendInput copy
  SendInput {Enter}
return


;Call Move function.
^#m::
  SendInput move
  SendInput {Enter}
return


;Call Trim function.
^#t::
  SendInput trim
  SendInput {Enter}
return


;Blow stuff up.
#^e::
  SendInput explode
  SendInput {enter}
return


;Noun/Verb Off.
#^-::
  SendInput pickfirst
  SendInput {enter}
  SendInput 0
  SendInput {enter}
return


;Noun/Verb on.
#^=::
  SendInput pickfirst
  SendInput {enter}
  SendInput 1
  SendInput {enter}
return


;Reset World Coords.
#^z::
  SendInput ucs
  SendInput {enter}
  SendInput w
  SendInput {enter}
return


;Reset Face Coords.
#z::
  SendInput ucs
  SendInput {enter}
  SendInput _v
  SendInput {enter}
return


;Reset to SW view
~!z::
  Send -VIEW
  SendInput {enter}
  Send SWISO
  SendInput {enter}
return


; 3D Polyline
#^;::
  SendInput _3dpoly
  SendInput {enter}
return


; Hide Object
#^NumpadSub::
  SendInput {Raw}_hideobjects
  SendInput {enter}
return


; Hide Object
#^NumpadAdd::
  SendInput {Raw}_unisolateobjects
  SendInput {enter}
return


; Hide Object
#^NumpadMult::
  SendInput {Raw}_isolateobjects
  SendInput {enter}
return


;Show all layers.
#NumpadAdd::
  SendInput _laythw
  SendInput {enter}
return


;Hide a layer.
#NumpadSub::
  SendInput _layfrz
  SendInput {enter}
return


;Move to -0.625.
#Numpad0::
  SendInput -.0625,-.0625
  SendInput {enter}
return


; Slow zoom
#^[::
  SendInput zoomfactor
  SendInput {enter}
  SendInput 3
  SendInput {enter}
return


; Normal zoom
#^]::
  SendInput zoomfactor
  SendInput {enter}
  SendInput 15
  SendInput {enter}
return
#IfWinActive


;<----------------------------------------------------------------------------->
;                        Quick common fractions to decimal
;<----------------------------------------------------------------------------->


#Numpad1::
  SendInput {Raw}.0625
  IfWinActive, Prompts
  {
    SendInput {Tab}
  }
  else
  {
    SendInput {Enter}
  }
return


#Numpad2::
  SendInput {Raw}.125
  IfWinActive, Prompts
  {
    SendInput {Tab}
  }
  else
  {
    SendInput {Enter}
  }
return


#Numpad3::
  SendInput {Raw}.25
  IfWinActive, Prompts
  {
    SendInput {Tab}
  }
  else
  {
    SendInput {Enter}
  }
return


#Numpad4::
  SendInput {Raw}.375
  IfWinActive, Prompts
  {
    SendInput {Tab}
  }
  else
  {
    SendInput {Enter}
  }
return


#Numpad5::
  SendInput {Raw}.5
  IfWinActive, Prompts
  {
    SendInput {Tab}
  }
  else
  {
    SendInput {Enter}
  }
return


#Numpad6::
  SendInput {Raw}.625
  IfWinActive, Prompts
  {
    SendInput {Tab}
  }
  else
  {
    SendInput {Enter}
  }
return


#Numpad7::
  SendInput {Raw}.75
  IfWinActive, Prompts
  {
    SendInput {Tab}
  }
  else
  {
    SendInput {Enter}
  }
return


#Numpad8::
  SendInput {Raw}.875
  IfWinActive, Prompts
  {
    SendInput {Tab}
  }
  else
  {
    SendInput {Enter}
  }
return


#Numpad9::
  SendInput {Raw}.9375
  IfWinActive, Prompts
  {
    SendInput {Tab}
  }
  else
  {
    SendInput {Enter}
  }
return


;<----------------------------------------------------------------------------->
;<                                Other Rad Things                             >
;<----------------------------------------------------------------------------->


^!-::
  ; Section header for text documents           ^---- This thing above
  ; The character to use for a comment (if any)
  ComChar := ""

  ; The style of the banner
  String1 := "<----------------------------------------------------------------------------->"
  String2 := "<                                                                             >"

  InputBox, What, What do you want it say?
  if ErrorLevel
    return
  else
    SendInput, {Raw}%ComChar%%String1%
    Sleep, 100
    Send, {Enter}
    diff2 := StrLen(What)
    diff1 := StrLen(String2)
    diff3 := Round((diff1-diff2)/2)

    StringLeft, ST1, String2, diff3
    If ((diff3*2)+StrLen(What) > StrLen(String2))
    {
      StringRight, ST2, String2, diff3-1
    } else {
      StringRight, ST2, String2, diff3
    }

    ;SendInput, %diff1% %diff2% %diff3%
    SendInput, {Raw}%ComChar%%ST1%%What%%ST2%
    Sleep, 100
    Send, {Enter}
    SendInput, {Raw}%ComChar%%String1%
    Sleep, 100
    SendInput, {Enter}
return


; Push enter X times
^#Enter::
  x = 0
  InputBox, Many, How many times
  if ErrorLevel
    return
  loop, %Many% {
    x++
    SendInput, {Enter}
    Sleep, 50
    ; MsgBox, %x%
  }
return


; Press enter 10 times  Ctrl+Enter
^Enter::
  SendInput, {Enter 11}
return


; Set material trim to .125 becuz i'm lazy like that
^Numpad1::
  Loop 4
  {
    SendInput, 0.125
    SendInput, {Tab}
  }
return
